<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading">
            <div class="page_heading_wrap">
                <div class="page_heading_content">
                    <h1>Регистрация</h1>
                </div>
            </div>
        </div>

        <section class="main_content">
            <div class="container">

                <div class="tabs">
                    <ul class="tabs_nav">
                        <li class="active"><a href="#" data-target=".tab1">Модуль планирования</a></li>
                        <li><a href="#" data-target=".tab2">Сценарный модуль</a></li>
                        <li><a href="#" data-target=".tab3">Сценарный модуль</a></li>
                    </ul>
                    <div class="white_box box_form">

                        <div class="tabs_item tab1 active">
                            <div class="box_form_row">
                                <div class="box_form_left">
                                    <div class="box_form_wrap">
                                        <form class="form">
                                            <div class="form_group">
                                                <input type="text" class="form_control form_control_email" name="emaim" placeholder="Введите ваш E-mail">
                                            </div>
                                            <div class="form_group">
                                                <input type="password" class="form_control form_control_password" name="pass" placeholder="Введите новый пароль">
                                            </div>
                                            <button type="submit" class="btn">Войти в систему</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="box_form_right">
                                    <div class="no_account">
                                        <span>Нет аккаунта на FilmToolz?</span>
                                        <a href="#">Зарегестрируйтесь</a>
                                    </div>
                                    <div class="social_enter">
                                        <div class="social_enter_text">
                                            Или войдите с помощью<br/>
                                            вашей соц.сети
                                        </div>
                                        <div class="form_social">
                                            <ul>
                                                <li><a class="vk" href="#"><i class="fa fa-vk"></i></a></li>
                                                <li><a class="gp" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a class="fb" href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a class="tw" href="#"><i class="fa fa-twitter"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tabs_item tab2">
                            <div class="box_form_row">
                                <div class="box_form_left">
                                    <div class="box_form_wrap">
                                        <form class="form">
                                            <div class="form_group">
                                                <input type="text" class="form_control form_control_email" name="emaim" placeholder="Введите ваш E-mail">
                                            </div>
                                            <div class="form_group">
                                                <input type="password" class="form_control form_control_password" name="pass" placeholder="Введите новый пароль">
                                            </div>
                                            <button type="submit" class="btn">Войти в систему</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="box_form_right">
                                    <div class="no_account">
                                        <span>Нет аккаунта на FilmToolz?</span>
                                        <a href="#">Зарегестрируйтесь</a>
                                    </div>
                                    <div class="social_enter">
                                        <div class="social_enter_text">
                                            Или войдите с помощью<br/>
                                            вашей соц.сети
                                        </div>
                                        <div class="form_social">
                                            <ul>
                                                <li><a class="vk" href="#"><i class="fa fa-vk"></i></a></li>
                                                <li><a class="gp" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a class="fb" href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a class="tw" href="#"><i class="fa fa-twitter"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tabs_item tab3">
                            <div class="box_form_row">
                                <div class="box_form_left">
                                    <div class="box_form_wrap">
                                        <form class="form">
                                            <div class="form_group">
                                                <input type="text" class="form_control form_control_email" name="emaim" placeholder="Введите ваш E-mail">
                                            </div>
                                            <div class="form_group">
                                                <input type="password" class="form_control form_control_password" name="pass" placeholder="Введите новый пароль">
                                            </div>
                                            <button type="submit" class="btn">Войти в систему</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="box_form_right">
                                    <div class="no_account">
                                        <span>Нет аккаунта на FilmToolz?</span>
                                        <a href="#">Зарегестрируйтесь</a>
                                    </div>
                                    <div class="social_enter">
                                        <div class="social_enter_text">
                                            Или войдите с помощью<br/>
                                            вашей соц.сети
                                        </div>
                                        <div class="form_social">
                                            <ul>
                                                <li><a class="vk" href="#"><i class="fa fa-vk"></i></a></li>
                                                <li><a class="gp" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a class="fb" href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a class="tw" href="#"><i class="fa fa-twitter"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>



            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
