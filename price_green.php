<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body class="page_green">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading heading_white">
            <div class="page_heading_wrap heading_small">

                <div class="page_heading_content">
                    <ul class="breadcrumb">
                        <li><a href="#">Модуль планирования</a></li>
                        <li><span>Цены</span></li>
                    </ul>
                    <h1>Цены</h1>
                </div>

            </div>
        </div>

        <section class="main_price">
            <div class="container">

                <table class="price mb_115">

                    <tr>
                        <th></th>
                        <td>
                            <div class="price_tariff price_tariff_one">
                                <div class="price_tariff_name">price</div>
                                <div class="price_tariff_new">Бесплатно</div>
                                <div class="price_tariff_date">до 01.07.2018</div>
                                <div class="price_tariff_old"><span>495 Р</span> в месяц</div>
                                <i class="price_tariff_logo">
                                    <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </div>
                        </td>
                        <td>
                            <div class="price_tariff price_tariff_two">
                                <div class="price_tariff_name">price</div>
                                <div class="price_tariff_new">Бесплатно</div>
                                <div class="price_tariff_date">до 01.07.2018</div>
                                <div class="price_tariff_old"><span>495 Р</span>  в месяц</div>
                                <i class="price_tariff_logo">
                                    <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </div>
                        </td>
                        <td>
                            <div class="price_tariff price_tariff_three">
                                <div class="price_tariff_name">price</div>
                                <div class="price_tariff_new">Бесплатно</div>
                                <div class="price_tariff_date">до 01.07.2018</div>
                                <div class="price_tariff_old"><span>495 Р</span> в месяц</div>
                                <i class="price_tariff_logo">
                                    <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Услуги входящие
                            в тарифный план
                        </th>
                        <td>
                            Оптимальный выбор
                            для индивидуальных
                            планировщиков
                        </td>
                        <td>
                            Расширенные функции
                            планирования проекта
                            с возможностью совместной
                            работы
                        </td>
                        <td>
                            Максимум функций,
                            возможность
                            использования системы всей
                            командой проекта
                        </td>
                    </tr>

                    <tr>
                        <th>Количество подключенных пользователей</th>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                    </tr>

                    <tr>
                        <th>Работа с либретто</th>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Аналитика</th>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Составление КПП</th>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Составление вызывных</th>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Планирование двумя группами</th>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Рассылка вызывных</th>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Ведение актерской занятости</th>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Фактический хронометраж и статус сцен</th>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Производственные отчеты</th>
                        <td></td>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Расчет гонораров актеров</th>
                        <td></td>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Отчетность по проекту (выработка, план-факт и др)</th>
                        <td></td>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                    <tr>
                        <th>Роли (права) пользователей, история изменений</th>
                        <td></td>
                        <td></td>
                        <td><img src="img/icon_check_green.png" alt=""></td>
                    </tr>

                </table>

                <div class="tariff_archive white_box">
                    <div class="tariff_archive_info">
                        <div class="tariff_archive_heading">Тариф архивный</div>
                        <p>При подключении тарифа «Архивный» у вас будет к нему доступ ещё на протяжении целого года,Без подключения проект удаляеться по истечению 6 месяцев после завершения проекта</p>
                    </div>
                    <div class="tariff_archive_text">
                        <div class="tariff_archive_heading"><strong>Архивный доступ</strong> к неиспользуемым проектам</div>
                        <div class="tariff_archive_price">990 Р в год</div>
                        <i class="tariff_archive_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </div>
                </div>

                <ul class="tariff_action">
                    <li>
                        Дарим<br/>
                        <strong>
                            приятные<br/>
                            скидки
                        </strong>
                    </li>
                    <li>
                        <strong>5%</strong>
                        <span>При единоразовой оплате на 6 месяцев</span>
                        <i class="tariff_action_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </li>
                    <li>
                        <strong>10%</strong>
                        <span>При единоразовой оплате на 12 месяцев</span>
                        <i class="tariff_action_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </li>
                </ul>

            </div>
        </section>

        <div class="main">
            <div class="container">
                <div class="payment">
                    <div class="payment_private">
                        <div class="payment_title">
                            Вы можете оплатить услуги<br/>
                            <strong>любым из удобных вам способов</strong>
                        </div>
                        <div class="payment_method">
                            <table>
                                <tr>
                                    <td>Банковские карты</td>
                                    <td>Электронные деньги</td>
                                    <td>Онлайн - банки</td>
                                    <td>Сотовые операторы</td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="images/logo/pay_logo_01.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_02.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_03.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_04.png" class="img-fluid">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="images/logo/pay_logo_05.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_06.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_07.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_08.png" class="img-fluid">
                                    </td>
                                <tr>
                                    <td>
                                        <img src="images/logo/pay_logo_09.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_10.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_11.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_12.png" class="img-fluid">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="images/logo/pay_logo_13.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_14.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_15.png" class="img-fluid">
                                    </td>
                                    <td>
                                        <img src="images/logo/pay_logo_16.png" class="img-fluid">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="payment_corp">
                        <div class="payment_title">Оплата <strong>по безналу</strong></div>
                        <div class="payment_lead">
                            Вы можете оплатить
                            FilmToolz по безналу,
                            заключив
                            с нами договор
                        </div>
                        <div class="payment_text">
                            Для этого пришлите
                            на <a href="#">info@filmtoolz.ru</a> учетную
                            карточку вашей организации,
                            ссылку на проект, укажите
                            планируемый период
                            использования — наш сотрудник
                            вышлет вам счет и активирует
                            доступ.
                        </div>
                        <i class="payment_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
