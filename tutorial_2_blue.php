<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body class="page_blue">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading">
            <div class="page_heading_wrap heading_large">

                <div class="page_heading_content">
                    <ul class="breadcrumb">
                        <li><a href="#">Модуль планирования</a></li>
                        <li><span>Обучение</span></li>
                    </ul>
                    <h1>Обучение</h1>
                    <div class="page_heading_text">В данном разделе вы можете изучить функционал системы, почитать <br/>статьи по основным вопросам, а также посмотреть видеоуроки</div>
                </div>

                <div class="heading_box">
                    <div class="heading_box_title"><strong>Есть вопросы </strong> по <br/>освоению программы?</div>
                    <div class="heading_box_text">Задайте вопрос почту и наши менеджеры ответят вам в течение нескольких часов</div>
                    <a class="heading_box_link" href="mailto:info@filmtoolz.ru">info@filmtoolz.ru</a>
                </div>

            </div>
        </div>

        <section class="main_content">
            <div class="container">

                <div class="mb_60"></div>

                <div class="white_box box_text mb_50">
                    <h3>1.	Создание проекта, управление проектом</h3>
                    <ul>
                        <li><span>1.1.</span>  Создание нового проекта</li>
                        <li><span>1.2.</span> Базовые настройки проекта</li>
                        <li><span>1.3.</span> Можно ли удалить проект?</li>
                        <li><span>1.4.</span> Можно ли создать копию текущего проекта?</li>
                        <li><span>1.5.</span> Как поместить проект в архив</li>
                    </ul>
                </div>

                <div class="white_box box_text mb_60">
                    <h3>2.	Добавление пользователей и настройка ролей</h3>
                    <ul>
                        <li><span>2.1.</span>  Создание нового проекта</li>
                        <li><span>2.2.</span> Базовые настройки проекта</li>
                        <li><span>2.3.</span> Можно ли удалить проект?</li>
                        <li><span>2.4.</span> Можно ли создать копию текущего проекта?</li>
                        <li><span>2.5.</span> Как поместить проект в архив</li>
                    </ul>
                </div>

                <div class="text-center">
                    <a href="#" class="btn_more">Показать еще уроки</a>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer_tutorial.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
