
$(".btn_modal").fancybox({
    'padding'    : 0
});

// IE11 SVG support
svg4everybody();

//--------

$(".module_nav li  a")
    .mouseover(function() {
        var box = $(this).closest('.module');
        var tab = $($(this).attr("data-target"));

        $(this).closest('.module_nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.module_item').removeClass('active');
        box.find(tab).addClass('active');
    })
    .mouseout(function(){

    });



var clients = new Swiper('.clients_slider', {
    slidesPerView: 5,
    spaceBetween: 0,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});
