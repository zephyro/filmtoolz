<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading">
            <div class="page_heading_wrap">
                <div class="page_heading_content">
                    <h1>О проекте</h1>
                </div>
            </div>
        </div>

        <section class="main pb_70">
            <div class="container">

                <div class="main_box mb_60">
                    <p>Filmtoolz – сервис, облегчающий работу съемочной группы на всех этапах, будь то планирование съемок, составление КПП, быстрое формирование вызывных, формирование производственной отчетности.</p>
                    <p>Filmtoolz был запущен в 2013 году – в нем было спланировано более 3 000 проектов, включая полнометражные фильмы, сериалы, короткие метры и рекламные ролики.</p>
                    <p>Сервис постоянно дорабатывается и развивается с учетом пожеланий, поступающих от вторых режиссеров, исполнительных продюсеров и других пользователей системы – мы рады обратной связи и всегда стараемся учитывать пожелания клиентов.</p>
                </div>

                <ul class="quote">
                    <li>
                        <div class="quote_image">
                            <img src="images/photo_01.png" class="img-fluid" alt="">
                        </div>
                    </li>
                    <li>
                        <div class="quote_author">
                            <strong>Олег Тиньков</strong>
                            <span>Создатель сервиса</span>
                        </div>
                    </li>
                    <li>
                        <div class="quote_title"><strong>Нам хотелось создать сервис</strong>, который потеснит сервис... Fuckbook</div>
                        <div class="quote_text">Нам хотелось создать сервис, который потеснит сервис знакомств Fuckbook и нам это удалось. Спасибо, что вы с нами</div>
                        <i class="quote_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </li>
                </ul>

            </div>
        </section>

        <section class="main main_gray">
            <div class="container">

                <h2 class="main_title text-center"><strong>Планируется</strong> запуск</h2>

                <div class="unit unit_green mb_60">
                    <div class="unit_left">

                        <div class="unit_heading">
                            <div class="unit_heading_logo">
                                <svg class="img-fluid" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="unit_heading_text">планирование</div>
                        </div>

                        <p>Сервис, упрощающий составление сметы проекта, план-фактный анализ расходов; ведение авансовых отчетов и бла-бла-бла</p>
                        <p>Сервис, упрощающий составление сметы проекта, план-фактный анализ расходов; ведение авансовых отчетов и бла-бла-бла</p>

                        <a href="#" class="btn btn_green">Участвовать в бета-тестировании</a>

                    </div>
                    <div class="unit_right">
                        <div class="unit_image">
                            <img src="img/unit_image_01.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>

                <div class="unit unit_blue">
                    <div class="unit_left">

                        <div class="unit_heading">
                            <div class="unit_heading_logo">
                                <svg class="img-fluid" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="unit_heading_text">планирование</div>
                        </div>

                        <p>Сервис, упрощающий составление сметы проекта, план-фактный анализ расходов; ведение авансовых отчетов и бла-бла-бла</p>
                        <p>Сервис, упрощающий составление сметы проекта, план-фактный анализ расходов; ведение авансовых отчетов и бла-бла-бла</p>

                        <a href="#" class="btn btn_blue">Участвовать в бета-тестировании</a>

                    </div>
                    <div class="unit_right">
                        <div class="unit_image">
                            <img src="img/unit_image_02.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
