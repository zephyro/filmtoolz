<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading">
            <div class="page_heading_wrap">
                <div class="page_heading_content">
                    <h1>Регистрация</h1>
                </div>
            </div>
        </div>

        <section class="main_content">
            <div class="container">

                <div class="white_box box_form">

                    <div class="box_form_row mb_40">
                        <div class="box_form_left">
                            <div class="form_title">Для регистрации введите адрес <strong>вашей электронной почты в форму</strong></div>
                        </div>
                        <div class="box_form_right">
                            <form class="form">
                                <ul class="box_inline_form">
                                    <li class="form_group">
                                        <input type="text" class="form_control form_control_email" name="email" placeholder="Введите ваш E-mail">
                                    </li>
                                    <li>
                                        <button type="submit" class="btn">Зарегистрироваться</button>
                                    </li>
                                </ul>
                            </form>
                            <div class="form_info_text">Письмо с новым паролем придёт к вам на почту в течении 3 минут</div>
                        </div>
                    </div>

                    <div class="box_form_row">
                        <div class="box_form_left">
                            <div class="form_title_second">Или зарегестрируйтесь через <br/>вашу социальную сеть</div>
                        </div>
                        <div class="box_form_right">
                            <div class="form_social">
                                <ul>
                                    <li><a class="vk" href="#"><i class="fa fa-vk"></i></a></li>
                                    <li><a class="gp" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a class="fb" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="tw" href="#"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
