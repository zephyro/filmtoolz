<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading">
            <div class="page_heading_wrap">
                <div class="page_heading_content">
                    <h1>Восстановление пароля</h1>
                </div>
            </div>
        </div>

        <section class="main_content">
            <div class="container">

                <div class="white_box box_form">
                    <form class="form">
                        <div class="box_form_row">
                            <div class="box_form_left">
                                <div class="form_title">Для восстановления введите адрес <strong>вашей электронной почты в форму</strong></div>
                            </div>
                            <div class="box_form_right">
                                <ul class="box_inline_form">
                                    <li class="form_group">
                                        <input type="text" class="form_control form_control_email" name="email" placeholder="Введите ваш E-mail">
                                    </li>
                                    <li>
                                        <button type="submit" class="btn">Восстановить пароль</button>
                                    </li>
                                </ul>
                                <div class="form_info_text">Письмо с новым паролем придёт к вам на почту в течении 3 минут</div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
