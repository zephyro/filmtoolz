<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading">
            <div class="page_heading_wrap">
                <div class="page_heading_content">
                    <h1>Настройки аккаунта</h1>
                </div>
                <ul class="page_nav">
                    <li class="active"><a href="#">Настройки аккаунта</a></li>
                    <li><a href="#">Проекты</a></li>
                    <li><a href="#">История платежей</a></li>
                </ul>
            </div>
        </div>

        <section class="main_content">
            <div class="container">

                <div class="white_box mb_80">
                    <form class="form">
                        <ul class="profile_content">
                            <li>
                                <div class="profile_email">
                                    <div class="profile_email_value">Ваш E-mail</div>
                                    <div class="profile_email_text">
                                        <div class="profile_email_inner">
                                            <strong>eatmollyagain@gmail.com</strong>
                                            <span>Вы не можете изменить это поле</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_box">
                                    <div class="form_group">
                                        <input type="text" class="form_control" name="name" placeholder="Введите ваше Имя">
                                        <div class="form_control_help"><span></span></div>
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control" name="lastName" placeholder="Введите вашу Фамилию">
                                        <div class="form_control_help"><span></span></div>
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control" name="secondName" placeholder="Введите ваше Отчество">
                                        <div class="form_control_help"><span>Необязательное поле</span></div>
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control" name="phone" placeholder="Введите номер вашего телефона">
                                        <div class="form_control_help"><span>Необязательное поле</span></div>
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control" name="company" placeholder="Введите название вашей Компании">
                                        <div class="form_control_help"><span>Необязательное поле</span></div>
                                    </div>
                                    <div class="form_button">
                                        <button type="submit" class="btn">Сохранить информацию</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="profile_file">
                                    <input type="file" name="file">
                                    <img src="img/file_photo.png" class="img-fluid" alt="">
                                    <span>Загрузить <br/>фотографию</span>
                                </label>
                            </li>
                        </ul>
                    </form>
                </div>

                <div class="auth_info">
                    <div class="auth_info_left">
                        <div class="form_title">Электронная почта<br/><strong>для уведомлений</strong></div>
                        <div class="form_wrap">
                            <div class="form_text">При изменении E-mail адреса необходимоподтверждение</div>
                            <form class="form">
                                <div class="form_group">
                                    <input type="text" class="form_control" name="" placeholder="eatmollyagain@gmail.com">
                                </div>
                                <button type="submit" class="btn">Сохранить новый E-mail</button>
                            </form>
                        </div>
                        <div class="form_social">
                            <div class="form_social_title">Привяжите ваш аккаунт<br/> <strong>к своим социальным сетям</strong></div>
                            <ul>
                                <li><a class="vk" href="#"><i class="fa fa-vk"></i></a></li>
                                <li><a class="gp" href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="fb" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="tw" href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="auth_info_right">
                        <div class="form_title">Изменение пароля</div>
                        <form class="form">
                            <div class="form_group">
                                <input type="text" class="form_control" name="pass" placeholder="Введите текущий пароль">
                            </div>
                            <div class="form_group">
                                <input type="text" class="form_control" name="pass" placeholder="Введите новый пароль">
                            </div>
                            <div class="form_group">
                                <input type="text" class="form_control" name="pass" placeholder="Повторите новый пароль">
                            </div>
                            <button type="submit" class="btn btn_gray">Изменить мой пароль</button>
                        </form>
                    </div>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->
    </body>

</html>
