<div class="email_block">
    <div class="container">
        <a href="#" class="email_link">info@filmtoolz.ru</a>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <a href="/" class="footer_logo">
            <i>
                <svg class="img-fluid" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </i>
        </a>
        <div class="footer_text"><span>Онлайн-сервисы<br/>для киноиндустрии </span></div>
        <ul class="footer_nav">
            <li><a href="#"><span>О проекте</span></a></li>
            <li><a href="#"><span>Цены</span></a></li>
            <li><a href="#"><span>Обучение</span></a></li>
        </ul>
        <ul class="footer_buttons">
            <li><a href="#">Регистрация</a></li>
            <li><a href="#">Вход</a></li>
        </ul>
    </div>
</footer>