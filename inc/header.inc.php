<header class="header">
    <div class="container">
        <a href="/" class="header_logo">
            <i>
                <svg class="img-fluid" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </i>
        </a>
        <div class="header_text"><span>Онлайн-сервисы<br/>для киноиндустрии </span></div>
        <ul class="header_nav">
            <li><a href="#"><span>О проекте</span></a></li>
            <li>
                <a href="#" class="dropdown"><span>Цены</span></a>
                <ul class="dropnav">
                    <li><a href="#"><span>Модуль планирования</span></a></li>
                    <li><a href="#"><span>Модуль бюджетирования</span></a></li>
                    <li><a href="#"><span>Сценарный модуль</span></a></li>
                </ul>
            </li>
            <li><a href="#" class="dropdown"><span>Обучение</span></a></li>
        </ul>
        <ul class="header_buttons">
            <li><a href="#">Регистрация</a></li>
            <li><a href="#">Вход</a></li>
        </ul>
    </div>
</header>