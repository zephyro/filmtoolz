<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="offer">
            <div class="container">
                <div class="offer_title"><strong>Сократите</strong> издержки кинопроизводства</div>
                <div class="offer_text">за счет детального планирования<br/>и менеджмента в онлайн сервисах для кино</div>
                <div class="offer_image">
                    <img src="img/offer__image.png" class="img-fluid" alt="">
                </div>
                <div class="offer_form">
                    <form class="form">
                        <ul class="offer_form_row">
                            <li>
                                <input type="text" class="form_control form_control_no_border" name="email" placeholder="Ваша почта">
                            </li>
                            <li>
                                <button type="submit" class="btn btn_send">Начать работу</button>
                            </li>
                        </ul>
                    </form>
                </div>

                <div class="offer_social">
                    <div class="offer_social_text">Или войдите через соц. сети</div>
                    <ul class="offer_social_elem">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </section>

        <div class="heading heading_shadow">
            <div class="container">
                <h2><strong>Модули</strong> FilmToolz</h2>
            </div>
        </div>

        <div class="heading heading_violet">
            <div class="container">
                <h2><strong>Модуль</strong> планирования</h2>
            </div>
        </div>

        <section class="module module_violet">
            <div class="container">

                <div class="module_main">

                    <div class="module_side">

                        <div class="module_heading">
                            <div class="module_heading_logo">
                                <svg class="ico-svg" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="module_heading_text">планирование</div>
                        </div>
                        <ul class="module_nav">
                            <li><a href="#" data-target=".item_01"><strong>Планирование</strong> КПП и выездных</a></li>
                            <li class="active"><a href="#" data-target=".item_02"><strong>Производственная</strong> отчетность</a></li>
                            <li><a href="#" data-target=".item_03"><strong>Хранение</strong> медиа-данных проектов</a></li>
                            <li><a href="#" data-target=".item_04"><strong>Актерская</strong> занятость</a></li>
                            <li><a href="#" data-target=".item_05"><strong>Аналитика</strong> съемок</a></li>
                            <li><a href="#" data-target=".item_06"><strong>Планирование</strong> КПП и выездных</a></li>
                            <li><a href="#" data-target=".item_07"><strong>Производственная</strong> отчетность</a></li>
                        </ul>

                    </div>

                    <div class="module_right">
                        <div class="module_content">
                            <div class="module_item item_01">
                                <div class="module_image">
                                    <img src="img/module_item_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_02 active">
                                <div class="module_image">
                                    <img src="img/module_item_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_03">
                                <div class="module_image">
                                    <img src="img/module_item_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_04">
                                <div class="module_image">
                                    <img src="img/module_item_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_05">
                                <div class="module_image">
                                    <img src="img/module_item_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_06">
                                <div class="module_image">
                                    <img src="img/module_item_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_07">
                                <div class="module_image">
                                    <img src="img/module_item_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <div class="heading heading_green heading_md">
            <div class="container">
                <h2><strong>Модуль</strong> бюджетирования</h2>
            </div>
        </div>

        <section class="module module_green">
            <div class="container">
                <div class="module_main">
                    <div class="module_side">
                        <div class="module_heading">
                            <div class="module_heading_logo">
                                <svg class="ico-svg" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="module_heading_text">бюджетирование (бета)</div>
                        </div>
                        <ul class="module_nav">
                            <li class="active"><a href="#" data-target=".item_01"><strong>Планирование</strong> КПП и выездных</a></li>
                            <li><a href="#" data-target=".item_02"><strong>Производственная</strong> отчетность</a></li>
                            <li><a href="#" data-target=".item_03"><strong>Хранение</strong> медиа-данных проектов</a></li>
                            <li><a href="#" data-target=".item_04"><strong>Актерская</strong> занятость</a></li>
                            <li><a href="#" data-target=".item_05"><strong>Аналитика</strong> съемок</a></li>
                            <li><a href="#" data-target=".item_06"><strong>Планирование</strong> КПП и выездных</a></li>
                            <li><a href="#" data-target=".item_07"><strong>Производственная</strong> отчетность</a></li>
                        </ul>
                    </div>
                    <div class="module_right">
                        <div class="module_content">
                            <div class="module_item item_01 active">
                                <div class="module_image">
                                    <img src="img/module_item_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_02">
                                <div class="module_image">
                                    <img src="img/module_item_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_03">
                                <div class="module_image">
                                    <img src="img/module_item_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_04">
                                <div class="module_image">
                                    <img src="img/module_item_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_05">
                                <div class="module_image">
                                    <img src="img/module_item_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_06">
                                <div class="module_image">
                                    <img src="img/module_item_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                            <div class="module_item item_07">
                                <div class="module_image">
                                    <img src="img/module_item_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="module_text"><strong>Возможность создания</strong> собственных шаблонов смет и работы с готовыми формами крупнейших кинокомпаний, календарное планирование </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="soon">
            <div class="container">
                <div class="soon_title"><strong>Планируется</strong> запуск</div>

                <ul class="soon_item soon_item_blue">
                    <li>
                       <div class="soon_name">
                           <i>
                               <svg class="ico-svg" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                                   <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                               </svg>
                           </i>
                           <span>сценарный модуль</span>
                       </div>
                    </li>
                    <li>
                        <div class="soon_plan">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 42 51" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite-icons.svg#icon_look" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <span>Планируется запуск сценарного модуля в январе 2019 года</span>
                        </div>
                    </li>
                </ul>

                <ul class="soon_item soon_item_red">
                    <li>
                        <div class="soon_name">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 428.05 76.48" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/logo.svg#icon_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <span>кастинг модуль</span>
                        </div>
                    </li>
                    <li>
                        <div class="soon_plan">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 42 51" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite-icons.svg#icon_look" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <span>Планируется запуск сценарного модуля в январе 2019 года</span>
                        </div>
                    </li>
                </ul>

            </div>
        </div>

        <div class="reviews">
            <div class="container">
                <div class="reviews_title"><strong>Отзывы</strong> наших клиентов</div>

                <ul class="review">
                    <li>
                        <div class="review_info">
                            <div class="review_info_date"><span>2017</span></div>
                            <div class="review_info_photo">
                                <img src="images/reivew_photo_01.png" class="img-fluid" alt="">
                            </div>
                            <div class="review_info_name">
                                <strong>Юлия Лазарева</strong>
                                <span>Второй режиссер</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4><strong>Планировать съемки</strong>, просто перетаскивая сцены мышкой...</h4>
                        <p>«Я люблю новые технологии. Облачные сервисы, мобильные приложения - все они сильно упрощают мне жизнь. Почему бы не применять их в работе? </p>
                        <i class="review_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </li>
                </ul>

                <ul class="review">
                    <li>
                        <div class="review_info">
                            <div class="review_info_date"><span>2017</span></div>
                            <div class="review_info_photo">
                                <img src="images/reivew_photo_02.png" class="img-fluid" alt="">
                            </div>
                            <div class="review_info_name">
                                <strong>Юлия Лазарева</strong>
                                <span>Второй режиссер</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4><strong>Планировать съемки</strong>, просто перетаскивая сцены мышкой...</h4>
                        <p>«Я люблю новые технологии. Облачные сервисы, мобильные приложения - все они сильно упрощают мне жизнь. Почему бы не применять их в работе? </p>
                        <i class="review_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </li>
                </ul>

                <ul class="review">
                    <li>
                        <div class="review_info">
                            <div class="review_info_date"><span>2017</span></div>
                            <div class="review_info_photo">
                                <img src="images/reivew_photo_01.png" class="img-fluid" alt="">
                            </div>
                            <div class="review_info_name">
                                <strong>Юлия Лазарева</strong>
                                <span>Второй режиссер</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4><strong>Планировать съемки</strong>, просто перетаскивая сцены мышкой...</h4>
                        <p>«Я люблю новые технологии. Облачные сервисы, мобильные приложения - все они сильно упрощают мне жизнь. Почему бы не применять их в работе? </p>
                        <i class="review_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </li>
                </ul>

                <ul class="review">
                    <li>
                        <div class="review_info">
                            <div class="review_info_date"><span>2017</span></div>
                            <div class="review_info_photo">
                                <img src="images/reivew_photo_02.png" class="img-fluid" alt="">
                            </div>
                            <div class="review_info_name">
                                <strong>Юлия Лазарева</strong>
                                <span>Второй режиссер</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h4><strong>Планировать съемки</strong>, просто перетаскивая сцены мышкой...</h4>
                        <p>«Я люблю новые технологии. Облачные сервисы, мобильные приложения - все они сильно упрощают мне жизнь. Почему бы не применять их в работе? </p>
                        <i class="review_logo">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </li>
                </ul>

                <div class="text-center">
                    <a href="#" class="review_view">Показать еще отзывы</a>
                </div>

            </div>
        </div>

        <div class="clients">
            <div class="container">
                <div class="clients_title"><strong>FilmToolz</strong> используют<br/> ведущие кинокомпании</div>
                <div class="clients_wrap">
                    <div class="clients_slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="client">
                                    <div class="client_image">
                                        <img src="images/clients_logo_01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="client_text">
                                        <strong>Yellow, Black and White</strong>
                                        <span>с 2016 года</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="client">
                                    <div class="client_image">
                                        <img src="images/clients_logo_02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="client_text">
                                        <strong>ИКа фильм</strong>
                                        <span>с 2016 года</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="client">
                                    <div class="client_image">
                                        <img src="images/clients_logo_03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="client_text">
                                        <strong>Амедиа</strong>
                                        <span>с 2013 года</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="client">
                                    <div class="client_image">
                                        <img src="images/clients_logo_04.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="client_text">
                                        <strong>ВГТРК</strong>
                                        <span>с 2012 года</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="client">
                                    <div class="client_image">
                                        <img src="images/clients_logo_02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="client_text">
                                        <strong>ИКа фильм</strong>
                                        <span>с 2016 года</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="client">
                                    <div class="client_image">
                                        <img src="images/clients_logo_02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="client_text">
                                        <strong>ИКа фильм</strong>
                                        <span>с 2016 года</span>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="client">
                                    <div class="client_image">
                                        <img src="images/clients_logo_03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="client_text">
                                        <strong>Амедиа</strong>
                                        <span>с 2013 года</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next">
                        <svg class="ico-svg" viewBox="0 0 17 36" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite-icons.svg#icon_slider_arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="swiper-button-prev">
                        <svg class="ico-svg" viewBox="0 0 17 36" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite-icons.svg#icon_slider_arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>

        <div class="news_block">
            <div class="container">
                <div class="news_block_title"><strong>Новости</strong> FilmToolz</div>
                <div class="news_row">

                    <a href="#" class="news_item">
                        <i class="news_item_icon">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <h4>Новые возможности FilmToolz</h4>
                        <div class="news_item_text">Добавлена новая функция для работы с КПП экспорт краткой версии КПП. Добавлена новая функция для работы с КПП экспорт краткой версии КПП</div>
                    </a>

                    <a href="#" class="news_item">
                        <i class="news_item_icon">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <h4>Новые возможности FilmToolz</h4>
                        <div class="news_item_text">Добавлена новая функция для работы с КПП экспорт краткой версии КПП. Добавлена новая функция для работы с КПП экспорт краткой версии КПП</div>
                    </a>

                    <a href="#" class="news_item">
                        <i class="news_item_icon">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <h4>Новые возможности FilmToolz</h4>
                        <div class="news_item_text">Добавлена новая функция для работы с КПП экспорт краткой версии КПП. Добавлена новая функция для работы с КПП экспорт краткой версии КПП</div>
                    </a>

                    <a href="#" class="news_item">
                        <i class="news_item_icon">
                            <svg class="ico-svg" viewBox="0 0 51.49 76.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon_mini_logo" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <h4>Новые возможности FilmToolz</h4>
                        <div class="news_item_text">Добавлена новая функция для работы с КПП экспорт краткой версии КПП. Добавлена новая функция для работы с КПП экспорт краткой версии КПП</div>
                    </a>

                </div>

                <div class="news_social">
                    <div class="news_social_title">Следите за новостями</div>
                    <ul>
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="progress">
            <div class="container">
                <ul>
                    <li>
                        <strong>3 579</strong>
                        <span>проектов спланировано при <br/>помощи FILMTOOLZ</span>
                    </li>
                    <li>
                        <strong>44 480</strong>
                        <span>смен спланировано при <br/>помощи FILMTOOLZ</span>
                    </li>
                    <li>
                        <strong>493 772</strong>
                        <span>сцены снято, благодаря <br/>планированию в FILMTOOLZ</span>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
