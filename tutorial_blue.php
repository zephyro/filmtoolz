<!doctype html>

<html class="no-js" lang="">

    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>

    <body class="page_blue">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <div class="page_heading">
            <div class="page_heading_wrap heading_large">

                <div class="page_heading_content">
                    <ul class="breadcrumb">
                        <li><a href="#">Модуль планирования</a></li>
                        <li><span>Обучение</span></li>
                    </ul>
                    <h1>Обучение</h1>
                    <div class="page_heading_text">В данном разделе вы можете изучить функционал системы, почитать <br/>статьи по основным вопросам, а также посмотреть видеоуроки</div>
                </div>

                <div class="heading_box">
                    <div class="heading_box_title"><strong>Есть вопросы </strong> по <br/>освоению программы?</div>
                    <div class="heading_box_text">Задайте вопрос почту и наши менеджеры ответят вам в течение нескольких часов</div>
                    <a class="heading_box_link" href="mailto:info@filmtoolz.ru">info@filmtoolz.ru</a>
                </div>

            </div>
        </div>

        <section class="main_content">
            <div class="container">
                <h2>2.	Добавление пользователей
                    <br/>и настройка ролей
                </h2>

                <div class="white_box tutorial_box mb_90">

                    <p class="text-center">Чтобы добавить нового пользователя, зайдите в раздел <strong>"Пользователи и роли"</strong></p>
                    <div class="tutorial_box_image">
                        <img src="images/screenshot_blue_01.jpg" class="img-fluid" alt="">
                        <div class="tutorial_box_hover" style="top: 95px; left: -76px;">
                            <img src="images/screenshot_01_blue_focus.jpg" class="img-fluid" alt="">
                        </div>
                    </div>

                    <p class="text-center">Нажмите кнопку <strong>«добавить»</strong></p>
                    <div class="tutorial_box_image">
                        <img src="images/screenshot_blue_02.jpg" class="img-fluid" alt="">
                        <div class="tutorial_box_hover" style="top: 335px; right: 15px;">
                            <img src="images/screenshot_02_blue_focus.jpg" class="img-fluid" alt="">
                        </div>
                    </div>

                    <p class="text-center">Посмотрите видео <strong>«Как добавить пользователя»</strong></p>
                    <div class="tutorial_box_image">
                        <a class="tutorial_video" data-fancybox="" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk">
                            <img src="images/screenshot_blue_03.jpg" class="img-fluid" alt="">
                        </a>
                    </div>

                    <div class="text-center">
                        <a href="#" class="btn btn_blue">Следущий урок</a>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn-back">Назад</a>
                    </div>
                </div>

                <div class="white_box box_text mb_60">
                    <h3>2.	Добавление пользователей и настройка ролей</h3>
                    <ul>
                        <li><span>1.1.</span>  Создание нового проекта</li>
                        <li><span>1.2.</span> Базовые настройки проекта</li>
                        <li><span>1.3.</span> Можно ли удалить проект?</li>
                        <li><span>1.4.</span> Можно ли создать копию текущего проекта?</li>
                        <li><span>1.5.</span> Как поместить проект в архив</li>
                    </ul>
                </div>

                <div class="text-center">
                    <a href="#" class="btn_more">Показать еще уроки</a>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer_tutorial.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
